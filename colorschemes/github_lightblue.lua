-- Originally written by thebirk, 2019
-- Updated by unsungNovelty in 2021 to reflect GitHub Lightblue theme
-- Colors are taken from https://github.com/primer/github-vscode-theme

local style = require "core.style"
local common = require "core.common"

style.background = { common.color "#ffffff" }
style.background2 = { common.color "#f2f2f2" }
style.background3 = { common.color "#f2f2f2" }
style.text = { common.color "#1b1f23" }
style.caret = { common.color "#24292e" }
style.accent = { common.color "#0366d6" }
style.dim = { common.color "#b0b0b0" }
style.divider = { common.color "#d0d7de" }
style.selection = { common.color "#b7dce8" }
style.line_number = { common.color "#57606a" }
style.line_number2 = { common.color "#24292f" }
style.line_highlight = { common.color "#f2f2f2" }
style.scrollbar = { common.color "#e0e0e0" }
style.scrollbar2 = { common.color "#c0c0c0" }

style.syntax["normal"] = { common.color "#24292f" }
style.syntax["symbol"] = { common.color "#24292f" } 
style.syntax["comment"] = { common.color "#6e7781" }
style.syntax["keyword"] = { common.color "#cb2431" }
style.syntax["keyword2"] = { common.color "#116329" }
style.syntax["number"] = { common.color "#24292e" }
style.syntax["literal"] = { common.color "#0366d6" }
style.syntax["string"] = { common.color "#044289" }
style.syntax["operator"] = { common.color "#1b1f23" }
style.syntax["function"] = { common.color "#8250df" }
