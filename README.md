# LiteXL files

Files related to LiteXL text editor

- [`github_lightblue.lua`](https://gitlab.com/unsungNovelty/litexl-files/-/blob/main/colorschemes/github_lightblue.lua) file contains the theme GitHub Light Blue theme for LiteXL text editor.
